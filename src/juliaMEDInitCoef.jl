module juliaMEDInitCoef

using Pkg.Artifacts

function getInitCoefPath()
	artifact_toml = joinpath(@__DIR__, "Artifacts.toml")
	println(@__DIR__)
	coeff_hash = artifact_hash("InitCoef", artifact_toml)
	if coeff_hash == nothing || !artifact_exists(coeff_hash)
	    coeff_hash = create_artifact() do artifact_dir
		InitCoef_URL = "https://gitlab.com/chansta/juliamedinitcoef/-/raw/master/resources"
		download("$(InitCoef_URL)/coefficients-K02.csv", joinpath(artifact_dir, "coefficients-K02.csv"))
	    end
	    bind_artifact!(artifact_toml, "InitCoef", coeff_hash)
	end
end

function __init__()
    getInitCoefPath()
end

end # module
