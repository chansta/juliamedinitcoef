# JuliaMEDInitCoef

This module installed the artifacts required by [juliaMED](https://gitlab.com/chansta/juliamed) to generate the initial value. This should be installed automatically during the installation process of juliaMED assuming all dependences are switch on. Otherwise, this should be installed manually before installing juliaMED. 


